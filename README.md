# dl-integrity-verify

A script to automatically download and perform integrity verifications for various pre-programmed pieces of software.

This was created primarily for my own usage, to remove the need to manually download and verify some of the software packages that I use. I have a Windows 10 machine at work, which is why some Windows tools are supported as well.

## Changelog

| Date | Notes |
|------|--------|
| **2020-06-19** | Updated Pop!_OS name to avoid file system naming errors, updated Pop build number from 6 to 9. |
| **2020-05-30** | Updated Pop!_OS ISO build from 5 to 6. |
| **2020-05-02** | Added Pop!_OS, updated desktop Ubuntus to 20.04. |
| **2020-03-15** | Added Ubuntu Server 18.04 LTS. |
| **2020-03-09** | Added Lubuntu 18.04.4 LTS. |
| **2019-06-18** | Polish and minor changes. |
| **2019-06-16** | Added additional verification methods, added auto-detection of GnuPG version, other minor changes/fixes and polish. |
| **2019-06-10** | Added support for selecting GnuPG versions, added signing keys, other minor changes. |
| **2019-06-09** | Initial pre-production release. |

## Supported Packages

Currently, the following software packages are supported (all 64-bit, where possible):

* Ubuntu 20.04 LTS (ISO)
* Ubuntu Server 18.04 LTS (ISO)
* Xubuntu 20.04 LTS (ISO)
* Lubuntu 20.0.4 LTS (ISO)
* Pop!_OS 20.04 LTS Intel (ISO)
* Kali Linux (ISO)
* Cygwin (EXE)
* KeePassXC (MSI)
* Nmap (EXE)
* Notepad++ (EXE)
* VirtualBox (EXE)
* Wireshark (EXE) - (Automatic integrity verification for Wireshark is not yet supported, coming soon.)

## Dependencies

* Bash
* Wget
* GnuPG (both `gpg2` and `gpg` are supported, see command-line arguments below)
* Whiptail OR Dialog

## Compatibility

The script will run in any standard Bash shell environment, as long as the dependencies above are met. Native GNU/Linux, Cygwin, macOS, *BSD, etc should all be supported.

## Running the Script

Ensure that your current working directory is that of the script, and the `sites/` and `downloads/` directories are present.

The `sites/` directory contains the URLs/origins of the downloadable files. For example, the download 'site' for the Ubuntu ISOs is `http://releases.ubuntu.com/`. These are separated from the main script to help prevent accidental modification.

Run the script using `./dl-integrity-verify.sh` or `bash dl-integrity-verify.sh`.

A menu will appear, follow the on-screen instructions to select a package to download.

The package will then download, and the integrity verifications will be performed automatically. The log will indicate whether the verifications passed, and the files will be saved to the `downloads/` directory. The log will also be saved to the `VERIFICATIONS` file.

## Command-line Arguments

* `--no-download` | `-nd`: Force the script to run without downloading anything. This is useful if you just want to verify files that are already downloaded.
* `--gpg2`: Force the script to use `gpg2` (GnuPG 2.x).
* `--gpg`: Force the script to use `gpg` (GnuPG 1.x).

## GnuPG Support

By default, the script will use `gpg2` (GnuPG 2.x) for signature verifications, unless only `gpg` (GnuPG 1.x) is available. If you'd like to force the usage of `gpg` (GnuPG 1.x), you can use the `--gpg` option, as described above.

## Included Signing Keys

The `signing-keys/` directory includes all of the required GPG signing keys. I have verified the fingerprints, however **you should also do this yourself! Never use a GPG key without first verifying its authenticity!**
