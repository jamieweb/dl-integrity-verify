#!/bin/bash
#dl-integrity-verify (Download & Verify) by Jamie Scaife (jamieweb.net) created 2019-05-26 23:00

#Check GnuPG 2.x/GnuPG 1.x availability
if (command -v gpg2 > /dev/null); then
  gnupg="gpg2"
  echo "Using GnuPG 2.x."
elif (command -v gpg > /dev/null); then
  gnupg="gpg"
  echo "Using GnuPG 1.x."
else
  echo "GnuPG 2.x or GnuPG 1.x are not installed. Please install 'gpg2' (preferred) or 'gpg'. Exiting..."
  exit
fi

#Check Whiptail/Dialog availability
if (command -v whiptail > /dev/null); then
  menu="whiptail"
elif (command -v dialog > /dev/null); then
  menu="dialog"
  echo "Using Dialog, as Whiptail doesn't appear to be installed."
else
  echo "Whiptail or Dialog are not installed. Please install 'whiptail' (preferred) or 'dialog'. Exiting..."
  exit
fi

#Parse command-line arguments
while [[ $# -gt 0 ]]; do
  argument="$1"
  case "$argument" in
    -nd|--no-download) #Force script to continue without downloading anything
      no_download=1
      shift
      ;;

    --gpg2) #Force usage of gpg2
      gnupg="gpg2"
      shift
      ;;

    --gpg) #Force usage of gpg
      gnupg="gpg"
      shift
      ;;

    *)
      echo "Invalid argument. Exiting..."
      exit
      ;;
  esac
done

#Log to file and stdout, unless '--noout' is set (it will only log to file in this case)
log () {
  if [ "$1" == "--noout" ]; then
    cat - >> "$log_file"
  else
    tee --output-error=exit -a "$log_file"
  fi
}

#Confirm user choice after selecting package, and create download directories accordingly
confirm_choice () {
  if [ -z "$choice" ]; then
    echo "No option selected, exiting..."
    exit
  elif ("$menu" --clear --title "Confirm Choice" --yesno "You selected '$1', are you sure?" 13 55); then
    mkdir -p "downloads/$choice"
    if [ "$?" != 0 ]; then
      echo "Error creating directory 'downloads/$choice', exiting..."
      exit
    fi
    log_file="downloads/$choice/VERIFICATIONS"
    echo "Saving log to '$log_file'" #This line is not logged.
    if [ -f "$log_file" ]; then
      printf "\n\n" | log --noout
    fi
    echo "`date`:" | log --noout
    echo "Selected '$choice'." | log
  else
    echo "Declined choice, exiting..."
    exit
  fi
}

#Check the sha256sum of the stored download site string against a known-good hash stored in the package configuration variables
verify_site () {
  dl_site_safe=$(echo "$dl_site" | sed 's/[.]/[.]/g; s/:/[:]/g')
  echo -e "\nVerifying download site: '$dl_site_safe'" | log
  dl_site_sha256_actual=$(sha256sum <<< "$dl_site" | head -c 64)
  echo "Expected hash: $dl_site_sha256_expected" | log
  echo "Actual hash:   $dl_site_sha256_actual" | log
  if [ "$dl_site_sha256_expected" == "$dl_site_sha256_actual" ]; then
    echo -e "Download site check: OK\n" | log
  else
    echo "Download site check FAILED. Exiting..." | log
    exit
  fi
}

#Check for potential file clobbering, and perform action in first argument accordingly ('--exit' or '--warn')
check_clobber () {
  echo -e "\nChecking for existing files that may be overwritten by download:" | log
  for file in "${dl_files[@]}"; do
    printf "$file: " | log
    if [ -f "downloads/$choice/$file" ]; then
      echo "FOUND" | log
      found=1
    else
      echo "Not Found (OK)" | log
    fi
  done
  if [ ! -z "$found" ]; then
    if [ "$1" == "--exit" ]; then
       echo "File(s) already exist. Remove the files, or use the '--no-download' option to force the script to continue. Exiting..." | log
      exit
    else
      echo "File(s) already exist, but the '--no-download' option is set. Continuing..." | log
    fi
  elif [ "$no_download" == 1 ]; then
    echo "Proceeding without downloading anything, as the '--no-download' option is set. This will probably result in an error, as there are no files/signatures to verify."
  fi
}

#Download files using wget
download () {
  source "sites/$dl_site_name"
  verify_site
  echo "Downloading files..." | log
  for file in "${dl_files[@]}"; do
    echo "Downloading '$file'..." | log
    wget --no-clobber --directory-prefix="downloads/$choice/" "$dl_site$dl_path/$file"
    if [ "$?" != 0 ]; then
      echo "Error downloading '$file', exiting..." | log
      exit
    elif ! [ -s "downloads/$choice/$(basename $file)" ]; then
      echo "Error, file '$(basename $file)' is zero, exiting..." | log
      exit
    fi
  done
}

#Various verification functions are available
gpg_signed_hashes_sha256_sha1_md5 () {
  gpg_verify "downloads/$choice/SHA256SUMS.gpg" "downloads/$choice/SHA256SUMS"
  gpg_verify "downloads/$choice/SHA1SUMS.gpg" "downloads/$choice/SHA1SUMS"
  gpg_verify "downloads/$choice/MD5SUMS.gpg" "downloads/$choice/MD5SUMS"
  hash_verify "sha256" "SHA256SUMS" "downloads/$choice/"
  hash_verify "sha1" "SHA1SUMS" "downloads/$choice/"
  hash_verify "md5" "MD5SUMS" "downloads/$choice/"
}

gpg_signed_hashes_sha256_sha1 () {
  gpg_verify "downloads/$choice/SHA256SUMS.gpg" "downloads/$choice/SHA256SUMS"
  gpg_verify "downloads/$choice/SHA1SUMS.gpg" "downloads/$choice/SHA1SUMS"
  hash_verify "sha256" "SHA256SUMS" "downloads/$choice/"
  hash_verify "sha1" "SHA1SUMS" "downloads/$choice/"
}

gpg_signed_hashes_sha256 () {
  gpg_verify "downloads/$choice/SHA256SUMS.gpg" "downloads/$choice/SHA256SUMS"
  hash_verify "sha256" "SHA256SUMS" "downloads/$choice/"
}

gpg_signed_direct () {
  gpg_verify "downloads/$choice/$sig_name" "downloads/$choice/$file_name"
}

hashed_direct_sha256_md5 () {
  hash_verify "sha256" "SHA256SUMS" "downloads/$choice/"
  hash_verify "md5" "MD5SUMS" "downloads/$choice/"
}

wireshark_gpg_signed_message_with_hashes_sha256_sha1_md5 () {
  echo "Automatic verification for Wireshark is not yet available, due to their less-common format of integrity verification files."
}

#Verify a GPG signature
gpg_verify () { # <signature> <file>
  echo "Verifying '$2' using signature '$1':" | log
  gpg_verification_output=$("$gnupg" --verify "$1" "$2" 2>&1)
  gpg_verification_exit_code="$?"
  echo "$gpg_verification_output" | log
  if [ "$gpg_verification_exit_code" == 0 ]; then
    echo -e "SUCCESS\n" | log
  else
    echo "BAD SIGNATURE, exiting..." | log
    exit
  fi
}

#Check a hash
hash_verify () { # <algo> <file> <directory>
  echo -n "Checking $1: " | log
  case "$1" in
    "sha256")
      hash_verification_output=$(cd "$3" && sha256sum --check --strict --ignore-missing "$2" 2>&1)
      ;;

    "sha1")
      hash_verification_output=$(cd "$3" && sha1sum --check --strict --ignore-missing "$2" 2>&1)
      ;;

    "md5")
      hash_verification_output=$(cd "$3" && md5sum --check --strict --ignore-missing "$2" 2>&1)
      ;;

    *)
      echo "Script error - invalid hashing algorithm. Exiting..." | log
      exit
      ;;
  esac
  hash_verification_exit_code="$?"
  echo "$hash_verification_output" | log
  if [ "$hash_verification_exit_code" != 0 ]; then
    echo "Hash verification error. Exiting..." | log
    exit
  fi
  grep " [ *]$file_name\$" "downloads/$choice/$2" | log
}

#Select package to download and verify
choice=$("$menu" --clear --title "Download & Verify" --menu "Select a package:" 13 55 5 \
 "Ubuntu 20.04 LTS" "  (ISO)" \
 "Ubuntu Server 18.04 LTS" "  (ISO)" \
 "Xubuntu 20.04 LTS" "  (ISO)" \
 "Lubuntu 20.04 LTS" "  (ISO)" \
 "Pop OS 20.04 LTS Intel" "  (ISO)" \
 "Kali Linux 2019.2" "  (ISO)" \
 "Raspbian Buster Lite 2019-07-10" "  (ZIP)" \
 "Cygwin" "  (EXE)" \
 "KeePassXC 2.5.3" "  (MSI)" \
 "Nmap 7.70" "  (EXE)" \
 "Notepad++ 7.7" "  (EXE)" \
 "VirtualBox 6.0.8" "  (EXE)" \
 "Wireshark 3.0.2" "  (EXE)" 3>&2 2>&1 1>&3)

#Confirm choice
confirm_choice "$choice"

#Set package-specific configuration variables
case "$choice" in
  "Ubuntu 20.04 LTS")
    file_name="ubuntu-20.04-desktop-amd64.iso"
    dl_site_name="ubuntu-releases"
    dl_site_sha256_expected="256b912563ae525d387be32be68fa3f4be7226e5af99f881cf6ab94cacf51fd0"
    dl_path="20.04"
    dl_files=("$file_name" "SHA256SUMS" "SHA256SUMS.gpg" "SHA1SUMS" "SHA1SUMS.gpg" "MD5SUMS" "MD5SUMS.gpg")
    verif_method="gpg_signed_hashes_sha256_sha1_md5"
    ;;

  "Ubuntu Server 18.04 LTS")
    file_name="ubuntu-18.04.4-live-server-amd64.iso"
    dl_site_name="ubuntu-releases"
    dl_site_sha256_expected="256b912563ae525d387be32be68fa3f4be7226e5af99f881cf6ab94cacf51fd0"
    dl_path="18.04.4"
    dl_files=("$file_name" "SHA256SUMS" "SHA256SUMS.gpg" "SHA1SUMS" "SHA1SUMS.gpg" "MD5SUMS" "MD5SUMS.gpg")
    verif_method="gpg_signed_hashes_sha256_sha1_md5"
    ;;

  "Xubuntu 20.04 LTS")
    file_name="xubuntu-20.04-desktop-amd64.iso"
    dl_site_name="ubuntu-cdimages"
    dl_site_sha256_expected="f8a490304bd43649c91c700f2272c356db952fa1326e4cff51368951efeaf521"
    dl_path="xubuntu/releases/20.04/release"
    dl_files=("$file_name" "SHA256SUMS" "SHA256SUMS.gpg" "SHA1SUMS" "SHA1SUMS.gpg" "MD5SUMS" "MD5SUMS.gpg")
    verif_method="gpg_signed_hashes_sha256_sha1_md5"
    ;;

  "Lubuntu 20.04 LTS")
    file_name="lubuntu-20.04-desktop-amd64.iso"
    dl_site_name="ubuntu-cdimages"
    dl_site_sha256_expected="f8a490304bd43649c91c700f2272c356db952fa1326e4cff51368951efeaf521"
    dl_path="lubuntu/releases/20.04/release"
    dl_files=("$file_name" "SHA256SUMS" "SHA256SUMS.gpg" "SHA1SUMS" "SHA1SUMS.gpg" "MD5SUMS" "MD5SUMS.gpg")
    verif_method="gpg_signed_hashes_sha256_sha1_md5"
    ;;

  "Pop OS 20.04 LTS Intel")
    file_name="pop-os_20.04_amd64_intel_9.iso"
    dl_site_name="pop-os-digitalocean-spaces"
    dl_site_sha256_expected="e8575d0d536c51898bdd4700c1d2eeb8b4e2d3bcf41b6279333e548f42f942cf"
    dl_path="20.04/amd64/intel/9"
    dl_files=("$file_name" "SHA256SUMS" "SHA256SUMS.gpg")
    verif_method="gpg_signed_hashes_sha256"
    ;;

  "Kali Linux 2019.2")
    file_name="kali-linux-2019.2-amd64.iso"
    dl_site_name="kali-cdimage"
    dl_site_sha256_expected="1a6f0fdebd881aa60ece5076a99349e2e8f5bbf2735d77dc7ae2442979da7f80"
    dl_path="kali-2019.2"
    dl_files=("$file_name" "SHA256SUMS" "SHA256SUMS.gpg" "SHA1SUMS" "SHA1SUMS.gpg")
    verif_method="gpg_signed_hashes_sha256_sha1"
    ;;

  "Raspbian Buster Lite 2019-07-10")
    file_name="2019-07-10-raspbian-buster-lite.zip"
    sig_name="2019-07-10-raspbian-buster-lite.zip.sig"
    dl_site_name="raspberry-pi-downloads"
    dl_site_sha256_expected="f2b05067ba9f3d3d4d2906b60e2c2256b2f0ab844781b88821fb497c48d70287"
    dl_path="raspbian_lite/images/raspbian_lite-2019-07-12"
    dl_files=("$file_name" "$sig_name")
    verif_method="gpg_signed_direct"
    ;;

  "Cygwin")
    file_name="setup-x86_64.exe"
    sig_name="setup-x86_64.exe.sig"
    dl_site_name="cygwin"
    dl_site_sha256_expected="beba6f771796df99e487f70dd307f254b050d1b9923a4bfa1c7bf0ba8c7a21cd"
    dl_path=""
    dl_files=("$file_name" "$sig_name")
    verif_method="gpg_signed_direct"
    ;;

  "KeePassXC 2.5.3")
    file_name="KeePassXC-2.5.3-Win64.msi"
    sig_name="KeePassXC-2.5.3-Win64.msi.sig"
    dl_site_name="github-keepassxreboot"
    dl_site_sha256_expected="9a2bc7b6290f8bb569cc510cf56b8b847e102cf8f4d8b8f09127785a7b36b9a5"
    dl_path="2.5.3"
    dl_files=("$file_name" "$sig_name")
    verif_method="gpg_signed_direct"
    ;;

  "Nmap 7.70")
    file_name="nmap-7.70-setup.exe"
    sig_name="nmap-7.70-setup.exe.asc"
    dl_site_name="nmap-dist"
    dl_site_sha256_expected="506d1efe9a49d17dd9d73613bc4e2d305e9f8b1ad55caeca78d926a4a637d832"
    dl_path=""
    dl_sig_path="sigs"
    dl_files=("$file_name" "$dl_sig_path/$sig_name")
    verif_method="gpg_signed_direct"
    ;;

  "Notepad++ 7.7")
    file_name="npp.7.7.Installer.x64.exe"
    sig_name="npp.7.7.Installer.x64.exe.sig"
    dl_site_name="npp-repository"
    dl_site_sha256_expected="bb38a037264882fa19a0830a9857bd6c3a07868f7aff07d3782e1278057fa7f7"
    dl_path="7.x/7.7"
    dl_files=("$file_name" "$sig_name")
    verif_method="gpg_signed_direct"
    ;;

  "VirtualBox 6.0.8")
    file_name="VirtualBox-6.0.8-130520-Win.exe"
    dl_site_name="virtualbox-downloads"
    dl_site_sha256_expected="30da57bc00cfef94e80ed860c19cd0f0898193742d3c68f3616073dbb9314f38"
    dl_path="6.0.8"
    dl_files=("$file_name" SHA256SUMS MD5SUMS)
    verif_method="hashed_direct_sha256_md5"
    security_warning="VirtualBox doesn't provide GPG signed releases, so a network or site-level attacker could bypass this integrity verification. Also check the Authenticode signing for extra assurance."
    ;;

  "Wireshark 3.0.2")
    file_name="Wireshark-win64-3.0.2.exe"
    sig_name="SIGNATURES-3.0.2.txt"
    dl_site_name="wireshark-downloads"
    dl_site_sha256_expected="9d5e133d874e70434799b0fcd2e27f636226e1c247ad152ccd16f29825c39aec"
    dl_path=""
    dl_file_path="win64"
    dl_files=("$dl_file_path/$file_name" "$sig_name")
    verif_method="wireshark_gpg_signed_message_with_hashes_sha256_sha1_md5"
    ;;

  *)
    echo "No Option Selected, Exiting..."
    exit
    ;;
esac

#Check for potential clobbering
if [ "$no_download" == 1 ]; then
  check_clobber "--warn"
else
  check_clobber "--exit"
  #Download the package files
  download
fi
printf "\n" | log

#Select and perform the relevant verification method
#Could just call function names from variable here, but using a case statement to help prevent arbitrary function name calling, and for future expansion
case "$verif_method" in
  "gpg_signed_hashes_sha256_sha1_md5") #Ubuntu ISOs
    gpg_signed_hashes_sha256_sha1_md5
    ;;

  "gpg_signed_hashes_sha256_sha1") #Kali Linux ISOs
    gpg_signed_hashes_sha256_sha1
    ;;

  "gpg_signed_hashes_sha256") #Pop!_OS ISOs
    gpg_signed_hashes_sha256
    ;;

  "gpg_signed_direct")
    gpg_signed_direct
    ;;

  "hashed_direct_sha256_md5")
    hashed_direct_sha256_md5
    ;;

  "wireshark_gpg_signed_message_with_hashes_sha256_sha1_md5")
    wireshark_gpg_signed_message_with_hashes_sha256_sha1_md5
    ;;

  *)
    echo "Script error - invalid verification method. Exiting..." | log
    exit
    ;;
esac

#Output the security warning, if one is present for the selected package
if [ ! -z "$security_warning" ]; then
  echo -e "\nSECURITY WARNING: $security_warning" | log
fi
